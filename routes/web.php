<?php
use App\Http\Controllers\ListController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/list', [ListController::class,'index'])->name('list');
Route::get('/tambahlist', [ListController::class,'create'])->name('tambahlist');
Route::post('/simpanlist', [ListController::class,'store'])->name('simpanlist');
Route::get('/editlist/{id}', [ListController::class,'edit'])->name('editlist');
Route::put('/updatelist/{id}', [ListController::class,'update'])->name('updatelist');
Route::get('/deletelist/{id}', [ListController::class,'destroy'])->name('deletelist');
