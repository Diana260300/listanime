<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListanimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listanimes', function (Blueprint $table) {
            $table->id();
            $table->string('judul', 100);
            $table->date('tanggal_rilis');
            $table->string('creator', 100);
            $table->string('rating', 100);
            $table->string('genre', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listanimes');
    }
}
