<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('list')}}">List</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<section>
    <div class="container text-center">
        <h5 class="alert alert-dark text-center mt-1">Masukkan Data</h5>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-body container text-left">
                    <form action="{{ route('simpanlist')}}" method="post">
                        {{ csrf_field()}}
                        <div class="form-group">
                          <label>Judul</label>
                          <input type="text" class="form-control" id="judul" name="judul" placeholder="masukkan judul">
                        </div>
                        <div class="form-group">
                          <label>Tanggal Rilis </label>
                          <input type="date" class="form-control" id="tanggal_rilis" name="tanggal_rilis">
                        </div>
                        <div class="form-group">
                          <label>Creator</label>
                          <input type="text" class="form-control" id="creator" name="creator" placeholder="nama creator">
                        </div>
                        <div class="form-group">
                          <label>Rating</label>
                          <input type="text" class="form-control" id="rating" name="rating" placeholder="masukkan ratingnya">
                        </div>
                        <div class="form-group">
                          <label>Genre</label>
                          <input type="text" class="form-control" id="genre" name="genre" placeholder="masukkan genrenya">
                        </div>
                        <button type="submit" class="btn btn-dark">Submit</button>
                      </form>
                </div>
            </div>
        </div>
    </div> 
</section>
  
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>