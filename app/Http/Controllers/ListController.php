<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\listanime;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = listanime::all();
        return view('list', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahlist');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        listanime::create([
            'judul' => $request->judul,
            'tanggal_rilis' => $request->tanggal_rilis,
            'creator' => $request->creator,
            'rating' => $request->rating,
            'genre' => $request->genre,
        ]);
        return redirect('list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = listanime::find($id);
        return view('editlist', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = listanime::findorfail($id);
        $save= $data->update([
            'judul' => $request->judul,
            'tanggal_rilis' => $request->tanggal_rilis,
            'creator' => $request->creator,
            'rating' => $request->rating,
            'genre' => $request->genre,
        ]);
        if($save){
            $list = listanime::all();
            return redirect()->route('list', compact('list'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list = listanime::findorfail($id);
        $list->delete();
        return back();
    }
}
