<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class listanime extends Model
{
    protected $table ="listanimes";
    protected $primaryKey="id";
    protected $fillable = [
        'id','judul','tanggal_rilis','creator','rating','genre'];
}
